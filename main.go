package main

import (
        "fmt"
        "net/http"
)

func main() {
        http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
                fmt.Fprintf(w, "Hallo, Minh Dep Trai!")
        })

        err := http.ListenAndServe(":80", nil)
        if err != nil {
                panic(err)
        }
}

